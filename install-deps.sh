# up to you (me) if you want to run this as a file or copy paste at your leisure

# https://rvm.io
# rvm for the rubiess
curl -L https://get.rvm.io | bash -s stable --ruby

# homebrew!
# you need the code CLI tools YOU FOOL.
ruby <(curl -fsSkL raw.github.com/mxcl/homebrew/go/install)

# https://github.com/rupa/z
# z, oh how i love you
mkdir ~/code
cd ~/code
git clone https://github.com/rupa/z.git
chmod +x ~/code/z/z.sh
# also consider moving over your current .z file if possible. it's painful to rebuild :)

# for the c alias (syntax highlighted cat)
sudo easy_install Pygments
